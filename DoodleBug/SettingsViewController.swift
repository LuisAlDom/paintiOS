//
//  SettingsViewController.swift
//  DoodleBug
//
//  Created by IDS Comercial on 20/03/18.
//  Copyright © 2018 IDS Comercial. All rights reserved.
//

import UIKit

protocol SettingsViewControllerDelegate: class{//Creo es para hacer las variables globales
    
    func settingsViewControllerFinished(_ settingsViewController: SettingsViewController)
}

class SettingsViewController: UIViewController {
    
    var delegate: SettingsViewControllerDelegate?//Hace variable global creo
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var brushSlider: UISlider!
    @IBOutlet weak var brushLabel: UILabel!
    
    @IBOutlet weak var opacitySlider: UISlider!
    @IBOutlet weak var opacityLabel: UILabel!
    
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    @IBOutlet weak var redLabel: UILabel!
    @IBOutlet weak var greenLabel: UILabel!
    @IBOutlet weak var blueLabel: UILabel!
    
    var brushWidth: CGFloat = 5.0
    var red: CGFloat = 0.0
    var green: CGFloat = 0.0
    var blue: CGFloat = 0.0
    
    var opacity: CGFloat = 1.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        brushLabel.text = String(format: "Brush Size: %.0f", brushWidth)//Pone lo q tiene brushWidth desde inicio
        brushSlider.value = Float(brushWidth)//Pone lo de brushWidth desde q carga el view
        
        opacityLabel.text = String(format: "Opacity: %.1f", opacity)
        opacitySlider.value = Float(opacity)
        
        redSlider.value = Float(red * 255)
        greenSlider.value = Float(green * 255)
        blueSlider.value = Float(blue * 255)
        
        redLabel.text = String(format: "%.0f", redSlider.value)
        greenLabel.text = String(format: "%.0f", greenSlider.value)
        blueLabel.text = String(format: "%.0f", blueSlider.value)

    }
    
    override func viewDidAppear(_ animated: Bool) { //Yo lo creo y es para cuando aparesca el objeto
        self.updatepreview()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func exitView(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        self.delegate?.settingsViewControllerFinished(self)//Se guardan los valores aun cuando se mate la vista
        
    }
    
    @IBAction func brushSize(_ sender: Any) {
        
        brushWidth = CGFloat(round(brushSlider.value))  //Se coloca el valor del tamaño slider  a la variable
        brushLabel.text = String(format: "Brush Size: %.0f", brushWidth)  //Pone el valor en el texto del label, hace un parseo
        
        self.updatepreview()
        
    }
    
    @IBAction func opacityChange(_ sender: Any) {
        
        opacity = CGFloat(opacitySlider.value)
        opacityLabel.text = String(format: "Opacity: %.1f", opacity)
        
        self.updatepreview()
        
    }
    
    @IBAction func redColour(_ sender: Any) {
        
        red = CGFloat(redSlider.value / 255)
        redLabel.text = String(format: "%.0f", redSlider.value)//con esto se actualizara el valor en la etiqueta
        
        self.updatepreview()
        
    }
    
    @IBAction func greenColour(_ sender: Any) {
        
        green = CGFloat(greenSlider.value / 255)
        greenLabel.text = String(format: "%.0f", greenSlider.value)
        
        self.updatepreview()
        
    }
    
    @IBAction func blueColour(_ sender: Any) {
        
        blue = CGFloat(blueSlider.value / 255)
        blueLabel.text = String(format: "%.0f", blueSlider.value)
        
        self.updatepreview()
        
    }
    
    func updatepreview(){//Para ver el cuadrito y pintar ahi
        
        UIGraphicsBeginImageContext(imageView.frame.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.setLineCap(CGLineCap.round)
        context?.setLineWidth(brushWidth)
        
        context?.setStrokeColor(red: red, green: green, blue: blue, alpha: opacity)
        context?.move(to: CGPoint(x: 120.0, y: 120.0))
        context?.addLine(to: CGPoint(x: 120.0, y: 120.0))
        
        context?.strokePath()
        
        imageView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
}
