//
//  ViewController.swift
//  DoodleBug
//
//  Created by IDS Comercial on 15/03/18.
//  Copyright © 2018 IDS Comercial. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var secondImage: UIImageView!
    @IBOutlet weak var hideButton: UIButton!
    @IBOutlet weak var preSetStack: UIStackView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    var hideState = false
    
    var lastPoint = CGPoint.zero //Va a dar el ultimo punto donde se quedo como 0
    var swiped = false //Variable para que pintes
    
    
    @IBOutlet weak var label: UILabel!
    
    var red: CGFloat = 0.0
    var green: CGFloat = 0.0
    var blue: CGFloat = 0.0
    
    var brushWidth: CGFloat = 5.0
    
    var opacity: CGFloat = 1.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.brushSize()//Para q inicialice la app con lo que tiene la variable
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {//Funcion para empezar a pintar
        
        swiped = false
        
        if let touch = touches.first as UITouch!{//Va a detectar el primer punto donde tocas
            
            lastPoint = touch.location(in: self.view) //Para poner la ultima ubicacion conocida
            
        }
        
    }//Began
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        swiped = true  //Se activa para q pueda pintar
        
        if let touch = touches.first as  UITouch!{
            
            let currentPoint = touch.location(in: view) //Es el punto actual en el q se encuentra
            drawline(lastPoint, toPoint: currentPoint)
            
            lastPoint = currentPoint //hara q se actualice constantemente
            
        }
        
    }//Moved
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if !swiped{//Si paras de hacer swiped va a dibujar hasta last point
            
            drawline(lastPoint, toPoint: lastPoint)
            
        }
        
        UIGraphicsBeginImageContext(secondImage.frame.size)//Para que se ve la opacidad
        
        imageView.image?.draw(in: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height), blendMode: CGBlendMode.normal, alpha: 1.0)//Para q se ve la opacidad
        secondImage.image?.draw(in: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height), blendMode: CGBlendMode.normal, alpha: opacity)//Para que se vea la opacidad
        
        imageView.image = UIGraphicsGetImageFromCurrentImageContext()//Para que se ve la opacidad
        UIGraphicsEndImageContext()//Para que se ve la opacidad
        
        secondImage.image = nil//Para que se ve la opacidad
        
        
    }//ended
    
    
    func drawline(_ fromPoint: CGPoint, toPoint: CGPoint){//Es el siguiente punto despues del last para dibujar
        
        UIGraphicsBeginImageContext(view.frame.size) //Va a tomar los datos graficos como contexto
        let context = UIGraphicsGetCurrentContext()
        
        secondImage.image?.draw(in: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))//los parametros para la linea
        
        context?.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
        context?.addLine(to: CGPoint(x: toPoint.x, y: toPoint.y))
        
        context?.setLineCap(CGLineCap.round)//Tipo de linea, hay 3 tipos de linea
        context?.setLineWidth(brushWidth)//Grueso de la linea
        context?.setStrokeColor(red: red, green: green, blue: blue, alpha: opacity)
        context?.setBlendMode(CGBlendMode.normal)
        
        context?.strokePath() //Interaccion con lapantalla
        
        secondImage.image = UIGraphicsGetImageFromCurrentImageContext()
        secondImage.alpha = opacity
        UIGraphicsEndImageContext()
        
    }
    
    @IBAction func red(_ sender: Any) {
        
        (red, green, blue) = (255, 0 , 0) //Aqui se coloca en las variables los colores
        
    }
    
    @IBAction func green(_ sender: Any) {
        
        (red, green, blue) = (0, 255, 0)
        
    }
    
    @IBAction func blue(_ sender: Any) {
        
        (red, green, blue) = (0, 0, 255)
        
    }
    
    @IBAction func black(_ sender: Any) {
        
        (red, green, blue) = (0, 0, 0)
        
    }
    
    @IBAction func white(_ sender: Any) {
        
        (red, green, blue) = (255, 255, 255)
        
    }
    
    @IBAction func large(_ sender: Any) {
        
        brushWidth += 1
        self.brushSize()
        
    }
    
    @IBAction func small(_ sender: Any) {
        
        brushWidth -= 1
        self.brushSize()
        
    }
    
    func brushSize(){
        
        label.text = String(format: "%.0f", brushWidth) //Para colocar en una cadena una varible flotante
        
        if brushWidth == 100{
            
            plusButton.isEnabled = false //Desactivara el boton
            plusButton.alpha = 0.25
            
        } else if brushWidth == 1{
            
            minusButton.isEnabled = false
            minusButton.alpha = 0.25
            
        } else {
            
            plusButton.isEnabled = true
            minusButton.isEnabled = true
            plusButton.alpha = 1.0
            minusButton.alpha = 1.0
            
        }
        
    }
    
    @IBAction func reset(_ sender: Any) {
        
        imageView.image = nil
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) { //Segue programatico
        
        let settingsViewController = segue.destination as! SettingsViewController //A donde va el segue manda los dato
        settingsViewController.delegate = self
        settingsViewController.brushWidth = brushWidth
        settingsViewController.red = red
        settingsViewController.green = green
        settingsViewController.blue = blue
        settingsViewController.opacity = opacity
        
    }
    
    @IBAction func savePhoto(_ sender: Any) {
        
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.image?.draw(in: CGRect(x: 0, y: 0, width: imageView.frame.size.width, height: imageView.frame.size.height))
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let activity = UIActivityViewController(activityItems: [image!], applicationActivities: nil)
        
        present(activity, animated: true, completion: nil)  //Se coloca en el plist Privacy - Photo library Additions
        
    }
    
    @IBAction func hideReveal(_ sender: Any) {
        
        if hideState == false{
            
            preSetStack.isHidden = true
            resetButton.isHidden = true
            settingsButton.isHidden = true
            saveButton.isHidden = true
            
            hideButton.setTitle("Reveal", for: UIControlState.normal)
            
            hideButton.alpha = 0.2
            
            hideState = true
            
        }else{
            
            preSetStack.isHidden = false
            resetButton.isHidden = false
            settingsButton.isHidden = false
            saveButton.isHidden = false
            
            hideButton.setTitle("Hide", for: UIControlState.normal)
            
            hideButton.alpha = 1
            
            hideState = false
            
        }
        
    }
    
}


extension ViewController: SettingsViewControllerDelegate{//Tiene q ir hasta abajo de todo el codigo
    
    func settingsViewControllerFinished(_ settingsViewController: SettingsViewController){//Este settingsViewController se usa abajo
        
        self.brushWidth = settingsViewController.brushWidth //El settings es de misma funcion q se acaba de crear Se grabaran del view donde esta el protocol a este
        self.red = settingsViewController.red
        self.green = settingsViewController.green
        self.blue = settingsViewController.blue
        self.opacity = settingsViewController.opacity
        
        self.brushSize() //Este va solo asi por q es de la funcion q esta arriba ya viene el valor ahi igual aqui se graban los valores del view donde esta el protocol a este
        
    }
    
}
